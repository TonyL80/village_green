<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" , shrink-to-fit=no>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accueil Village Green</title>
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/pages_village_green/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link href="/pages_village_green/village_green.css" type="text/css" rel="stylesheet" media="screen,projection" />

</head>

<body>
<header>
    <!--  ____    ____  __ __  ____    ____  ____  
         |    \  /    ||  |  ||    \  /    ||    \ 
         |  _  ||  o  ||  |  ||  o  )|  o  ||  D  )
         |  |  ||     ||  |  ||     ||     ||    / 
         |  |  ||  _  ||  :  ||  O  ||  _  ||    \ 
         |  |  ||  |  | \   / |     ||  |  ||  .  \
         |__|__||__|__|  \_/  |_____||__|__||__|\_|
                                              -->

    <nav class="black" role="navigation">
        <div class="nav-wrapper container">
            <img id="logo-container" src="/ressources/HEADER/logo village green.png" class="brand-logo">
            <ul class="right hide-on-med-and-down">
                <li><a href="http://localhost:3000/pages_village_green/index.php">Accueil</a></li>
                <li><a href="#">Boutique</a></li>
                <li><a href="/pages_village_green/contact_village_green.html">Connexion/Inscription</a></li>
            </ul>

            <ul id="nav-mobile" class="sidenav">
                <li><a href="http://localhost:3000/pages_village_green/index.php">Accueil</a></li>
                <li><a href="#">Boutique</a></li>
                <li><a href="/pages_village_green/contact_village_green.html">Connexion/Inscription</a></li>
            </ul>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>


    <!-- 
____    ____  ____   ____     ___  ____  
|    \  /    ||    \ |    \   /  _]|    \ 
|  o  )|  o  ||  _  ||  _  | /  [_ |  D  )
|     ||     ||  |  ||  |  ||    _]|    / 
|  O  ||  _  ||  |  ||  |  ||   [_ |    \ 
|     ||  |  ||  |  ||  |  ||     ||  .  \
|_____||__|__||__|__||__|__||_____||__|\_|                                                                                                                            
 -->
    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <br><br>
                <h1 class="header center"><b>VILLAGE GREEN</b></h1>
            <div class="row center">
                <strong>
                    <h5 class="header col s12 light"> LA QUALITÉ AU SERIVCE DE LA MUSIQUE
                    </h5>
                </strong>
            </div>
            <div class="row center">
                <a href="http://localhost:3000/pages_village_green/inscription_village_green.html" id="contact-button"
                    class="btn-large">S'inscrire / Se Connecter</a>
            </div>
            <br>
        </div>
        <div class="parallax"><img src="/ressources/category_roll_over/guitare.jpg" alt="Unsplashed background img 1">
        </div>
    </div>
</header>
    <div class="container">
        <div class="section">

            <!--   Icon Section   -->
            <div class="row">
                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="material-icons">flash_on</i></h2>
                        <h5 class="center">Speeds up development</h5>

                        <p class="light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum
                            asperiores natus magnam consectetur. Consequuntur sed ipsa omnis veniam at fuga
                            pariatur commodi assumenda quibusdam voluptatibus, cum repellendus fugit
                            necessitatibus nesciunt.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="material-icons">group</i></h2>
                        <h5 class="center">User Experience Focused</h5>

                        <p class="light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum
                            asperiores natus magnam consectetur. Consequuntur sed ipsa omnis veniam at fuga
                            pariatur commodi assumenda quibusdam voluptatibus, cum repellendus fugit
                            necessitatibus nesciunt.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="material-icons">settings</i></h2>
                        <h5 class="center">Easy to work with</h5>

                        <p class="light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum
                            asperiores natus magnam consectetur. Consequuntur sed ipsa omnis veniam at fuga
                            pariatur commodi assumenda quibusdam voluptatibus, cum repellendus fugit
                            necessitatibus nesciunt.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h1 class="header col s12 light">A modern responsive front-end framework based on Material
                        Design</h1>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="/ressources/category_roll_over/accessoires.jpg" alt="Unsplashed background img 2"></div>
    </div>

    <div class="container">
        <div class="section">

            <div class="row">
                <div class="col s12 center">
                    <h3><i class="mdi-content-send brown-text"></i></h3>
                    <h4>Contact Us</h4>
                    <p class="left-align light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
                        scelerisque id nunc nec volutpat. Etiam pellentesque tristique arcu, non consequat magna
                        fermentum ac. Cras ut ultricies eros. Maecenas eros justo, ullamcorper a sapien id,
                        viverra ultrices eros. Morbi sem neque, posuere et pretium eget, bibendum sollicitudin
                        lacus. Aliquam eleifend sollicitudin diam, eu mattis nisl maximus sed. Nulla imperdiet
                        semper molestie. Morbi massa odio, condimentum sed ipsum ac, gravida ultrices erat.
                        Nullam eget dignissim mauris, non tristique erat. Vestibulum ante ipsum primis in
                        faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                </div>
            </div>

        </div>
    </div>


    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h1 class="header col s12 light">LOCATION DE MATERIEL POUR PROFESSIONNELS</h1>
                    <a href="#" id="contact-button" class="btn-large">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="/ressources/category_roll_over/flight cases.jpg" alt="Unsplashed background img 3"></div>
    </div>
    </div>
    <!-- FOOTER -->
    <footer class="page-footer teal">



        <!--   <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div> -->

        <!-- ------------------------------------------------ -->

        <div class="col s12 m8 offset-m2 l6 offset-l3" style=margin-left:2% style=margin-right:2%>

       <!--     <div class="card-panel teal"> -->



                <div class="container-fluid" style=margin-bottom:1%>
                    <div>

                        <A href="https://www.paypal.com/"> <img src="image/paypal.png" title="Site Paypal"> </A>
                        <A href="https://www.chronopost.fr/"> <img src="image/chronopost.png" title="Site Chronopost">
                        </A>
                        <A href="https://www.mastercard.fr/"> <img src="image/mastercard.png" title="Site MasterCard">
                        </A>
                        <A href="https://www.visa.fr/"> <img src="image/visa.png" title="Site Visa"> </A>

                    </div>
                </div>



                <!--        <div class="card-panel grey lighten-5 z-depth-1"> -->
                <!--  <div class="row valign-wrapper"> -->
                <div class="row">
                    <div class="col s3">
                        <span class="black-text">
                            <b>Contactez nous !</b>
                            Conseil/commande t�l�phone</br>
                            du lundi au vendredi de 8h � 19h </br>
                            le samedi de 10h � 18h</br>
                            </br>
                            Depuis la France</br>
                            <b>02 40 38 50 50</b> </br>
                            belgique, Suisse, International:</br>
                            <b>0033 2 40 38 50 50</b> </br> </br>
                            <b>Service apr�s vente</b> </br>
                            <b>02 51 80 68 76</b> </br>
                            Contactez-nous part t�l�phone </br>
                            du lundi au samedi de 9h � 18h </br>
                            ou depuis votre compte client </br> </br>
                            <b>Conseils Pour choisir</b> </br>
                            <b>un instrument: </b> </br>
                            infovg@villagegreen.com </br> </br>
                            <b>Service presse </b> </br>
                            Contact@villagegreen.com </br> <br>
                            <b>Village green Recrute ! </b> </br>
                        </span>
                    </div>
                    <div class="col s3">
                        <span class="black-text">
                            <b>Village Green Store</b>
                            <b>Ouvert de 10h � 19h30 non-stop</b></br>
                            Guitares - Amplificateurs - Effets: </br>
                            162 avenues Jean Jaur�s</br>
                            76010 Paris</br></br>

                            Clavier - Home Studio - Sono: </br>
                            Equipement DJ - Eclairage :</br>
                            164 avenues Jean Jaur�s</br>
                            76010 Paris</br></br>

                            Librairie Musicale: </br>
                            7 av. du nouveau Conservatoire :</br>
                            76010 Paris</br></br>

                            Instrument � vent: </br>
                            911 Av. du nouveau Conservatoire:</br>
                            76010 Paris</br></br>

                            Percussion: </br>
                            13/16 Av. du nouveau Conservatoire:</br>
                            76019 Paris </br></br>

                            M�tro Ligne 5, station Porte de pantin </br></br>
                            Plan d'acc�s visite virtuelle </br></br>
                        </span>
                    </div>
                    <div class="col s3">
                        <span class="black-text">
                            <b>Utiles ! </b></br></br>
                            Qui sommes nous</br></br>
                            F.A.Q </br>
                            Vous avez un site internet?</br></br>
                            Devenez partenaire Village Green </br></br>
                            Condition G�n�rales de vente </br>
                            Mentions l�gales </br></br>
                            Plan du sites </br></br>
                            Parrainage </br>
                            Nouveaut�s </br></br>
                            Assurance Woodbrass.com </br></br>
                            Location d'instruments de musique </br></br>
                        </span>
                    </div>
                    <div class="col s3">
                        <span class="black-text">
                            <b>Villa Green </b></br>
                            <b>Preservation Society</b></br></br>
                            Toute l'actualit� musicale </br></br>
                            Voir l'avis des musiciens </br></br>
                            Concours : inscription et r�</br></br>
                            � vous de jouer! </br>
                        </span>
                    </div>
                </div>
    <!--        </div> -->


        </div>

        <!-- ---------------------------------------------------- -->

       
        <div class="footer-copyright">
            <div class="container">
                Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Tony Leva</a>
            </div>
    </footer>


    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/pages_village_green\js\materialize.js"></script>
    <script src="/pages_village_green\js\init.js"></script>

</body>

</html>