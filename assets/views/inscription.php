<?php
include './header.php';
?>

<div class="row">
     <div class="col-12">
          <form method="POST" action="../js/control_connect.js">
               <input type="text" name="login" aria-placeholder="Votre identifiant" placeholder="Identifiant" />
               <input type="email" name="mail" aria-placeholder="Votre adresse mail" placeholder="Adresse Mail" />
               <input type="password" name="password" aria-placeholder="Votre mot de passe" placeholder="Mot de passe" />
               <input type="password" name="cpassword" aria-placeholder="Confirmer votre mot de passe" placeholder="Confirmer votre mot de passe" />
               <input type="submit" value="S'enregistrer" />
          </form>
     </div>
</div>

<?php
include './views/footer.php';
?>