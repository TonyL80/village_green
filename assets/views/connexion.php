<?php
include './header.php';
?>

<div class="row">
    <div class="col-12">
        <form method="POST" action="../js/control_connect.js">
            <input type="text" name="id" aria-placeholder="Votre identifiant" placeholder="Identifiant" />
            <input type="password" name="password" aria-placeholder="Votre mot de passe" placeholder="Mot de passe" />
            <input type="submit" value="Se connecter" />
        </form>
    </div>
</div>

<?php
include './views/footer.php';
?>