
    <!--  ____    ____  __ __  ____    ____  ____  
         |    \  /    ||  |  ||    \  /    ||    \ 
         |  _  ||  o  ||  |  ||  o  )|  o  ||  D  )
         |  |  ||     ||  |  ||     ||     ||    / 
         |  |  ||  _  ||  :  ||  O  ||  _  ||    \ 
         |  |  ||  |  | \   / |     ||  |  ||  .  \
         |__|__||__|__|  \_/  |_____||__|__||__|\_|
                                              -->

    <nav class="black" role="navigation">
        <div class="nav-wrapper container">
            <img id="logo-container" src="/ressources/HEADER/logo village green.png" class="brand-logo">
            <ul class="right hide-on-med-and-down">
                <li><a href="#">Accueil</a></li>
                <li><a href="#">Boutique</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Connexion</a></li>
            </ul>

            <ul id="nav-mobile" class="sidenav">
                <li><a href="#">Accueil</a></li>
                <li><a href="#">Boutique</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Connexion</a></li>
            </ul>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>

    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center">VILLAGE GREEN</h1>
                <div class="row center">
                    <strong>
                        <h5 class="header col s12 light">LA QUALITÉ AU SERIVCE DE LA MUSIQUE
                        </h5>
                    </strong>
                </div>
                <div class="row center">
                    <a href="#" id="contact-button" class="btn-large">Contact</a>
                </div>
                <br><br>

            </div>
        </div>
        <div class="parallax"><img src="/ressources/category_roll_over/guitare.jpg" alt="Unsplashed background img 1">
        </div>
    </div>


    <div class="container">
        <div class="section">