<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" , shrink-to-fit=no>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accueil Village Green</title>
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/pages_village_green/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
    <link href="/pages_village_green/village_green.css" type="text/css" rel="stylesheet" media="screen,projection" />

</head>

<body>
    <header>
        <!--  ____    ____  __ __  ____    ____  ____  
         |    \  /    ||  |  ||    \  /    ||    \ 
         |  _  ||  o  ||  |  ||  o  )|  o  ||  D  )
         |  |  ||     ||  |  ||     ||     ||    / 
         |  |  ||  _  ||  :  ||  O  ||  _  ||    \ 
         |  |  ||  |  | \   / |     ||  |  ||  .  \
         |__|__||__|__|  \_/  |_____||__|__||__|\_|
                                              -->

        <nav class="black" role="navigation">
            <div class="nav-wrapper container">
                <img id="logo-container" src="/ressources/HEADER/logo village green.png" class="brand-logo">
                <ul class="right hide-on-med-and-down">
                    <li><a href="#">Accueil</a></li>
                    <li><a href="#">Boutique</a></li>
                    <li><a href="http://localhost:3000/pages_village_green/inscription_village_green.html">Connexion/Inscription</a></li>
                </ul>

                <ul id="nav-mobile" class="sidenav">
                    <li><a href="http://localhost:3000/pages_village_green/index.php">Accueil</a></li>
                    <li><a href="#">Boutique</a></li>
                    <li><a href="http://localhost:3000/pages_village_green/inscription_village_green.html">Connexion/Inscription</a></li>
                </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>

        <!-- 
____    ____  ____   ____     ___  ____  
|    \  /    ||    \ |    \   /  _]|    \ 
|  o  )|  o  ||  _  ||  _  | /  [_ |  D  )
|     ||     ||  |  ||  |  ||    _]|    / 
|  O  ||  _  ||  |  ||  |  ||   [_ |    \ 
|     ||  |  ||  |  ||  |  ||     ||  .  \
|_____||__|__||__|__||__|__||_____||__|\_|                                                                                                                            
 -->
        <div id="index-banner" class="parallax-container">
            <div class="section no-pad-bot">
                <div class="container">
                    <br><br>
                    <b>
                        <h1 class="header center">VILLAGE GREEN</h1>
                    </b>
                    <div class="row center">
                        <strong>
                            <h5 class="header col s12 light"><b> LA QUALITÉ AU SERIVCE DE LA MUSIQUE</b>
                            </h5>
                        </strong>
                    </div>
                    <div class="row center">
                        <a href="http://localhost:3000/pages_village_green/inscription_village_green.html" id="contact-button" class="btn-large">S'inscrire / Se connecter</a>
                    </div>
                    <br><br>

                </div>
            </div>
            <div class="parallax"><img src="/ressources/category_roll_over/guitare.jpg" alt="Unsplashed background img 1">
            </div>
        </div>
    </header>
    <div class="container">
        <div class="section">

            <!--   Icon Section   -->
            <div class="row">
                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="material-icons">flash_on</i></h2>
                        <h5 class="center">Speeds up development</h5>

                        <p class="light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum
                            asperiores natus magnam consectetur. Consequuntur sed ipsa omnis veniam at fuga
                            pariatur commodi assumenda quibusdam voluptatibus, cum repellendus fugit
                            necessitatibus nesciunt.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="material-icons">group</i></h2>
                        <h5 class="center">User Experience Focused</h5>

                        <p class="light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum
                            asperiores natus magnam consectetur. Consequuntur sed ipsa omnis veniam at fuga
                            pariatur commodi assumenda quibusdam voluptatibus, cum repellendus fugit
                            necessitatibus nesciunt.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="material-icons">settings</i></h2>
                        <h5 class="center">Easy to work with</h5>

                        <p class="light">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum
                            asperiores natus magnam consectetur. Consequuntur sed ipsa omnis veniam at fuga
                            pariatur commodi assumenda quibusdam voluptatibus, cum repellendus fugit
                            necessitatibus nesciunt.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h1 class="header col s12 light">A modern responsive front-end framework based on Material
                        Design</h1>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="/ressources/category_roll_over/accessoires.jpg" alt="Unsplashed background img 2"></div>
    </div>

    <div class="container">
        <div class="section">

            <div class="row">
                <div class="col s12 center">
                    <h3><i class="mdi-content-send brown-text"></i></h3>
                    <h4>Contact Us</h4>
                    <p class="left-align light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
                        scelerisque id nunc nec volutpat. Etiam pellentesque tristique arcu, non consequat magna
                        fermentum ac. Cras ut ultricies eros. Maecenas eros justo, ullamcorper a sapien id,
                        viverra ultrices eros. Morbi sem neque, posuere et pretium eget, bibendum sollicitudin
                        lacus. Aliquam eleifend sollicitudin diam, eu mattis nisl maximus sed. Nulla imperdiet
                        semper molestie. Morbi massa odio, condimentum sed ipsum ac, gravida ultrices erat.
                        Nullam eget dignissim mauris, non tristique erat. Vestibulum ante ipsum primis in
                        faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                </div>
            </div>

        </div>
    </div>


    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h1 class="header col s12 light">LOCATION DE MATERIEL POUR CONCERTS</h1>
                    <a href="#" id="contact-button" class="btn-large">EN SAVOIR PLUS</a>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="/ressources/category_roll_over/flight cases.jpg" alt="Unsplashed background img 3"></div>
    </div>
    </div>
    <!-- FOOTER -->
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">A propos</h5>
                    <p class="grey-text text-lighten-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam
                        repellendus inventore atque eum, omnis vero corporis eius ipsa. Earum, ipsa culpa animi
                        repellat quos harum. Omnis facere earum quas eaque.</p>
                </div>
                <div class="col l3 s12">
                    <h5 class="white-text">Settings</h5>
                    <ul>
                        <li><a class="white-text" href="#!">Link 1</a></li>
                        <li><a class="white-text" href="#!">Link 2</a></li>
                        <li><a class="white-text" href="#!">Link 3</a></li>
                        <li><a class="white-text" href="#!">Link 4</a></li>
                    </ul>
                </div>
                <div class="col l3 s12">
                    <h5 class="white-text">Connect</h5>
                    <ul>
                        <li><a class="white-text" href="#!">Link 1</a></li>
                        <li><a class="white-text" href="#!">Link 2</a></li>
                        <li><a class="white-text" href="#!">Link 3</a></li>
                        <li><a class="white-text" href="#!">Link 4</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                Made by <a class="brown-text text-lighten-3" href="http://materializecss.com">Tony Leva</a>
            </div>
    </footer>


    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="/pages_village_green\js\materialize.js"></script>
    <script src="/pages_village_green\js\init.js"></script>

</body>

</html>